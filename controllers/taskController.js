//Controllers contain the functions and business logic of our Express JS application

//import model
//Allow us to use the contents of the "Task.js" file in the models folder
const Task = require("../models/Task");

//Controller function for getting all the tasks
module.exports.getAllTasks = () => {

	return Task.find({}).then(result => {
		return result
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})
		//newTask.save((error, task)=>{})
		return newTask.save().then((task, error)=>{
			if (error) {
				console.log(error)
				return false
			} else {
				return task
			}
		})
}
//taskId- parameter name
//findByAndRemove -mongoose methods -search id and then remove or delete the documents in our database
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err)=> {
		if(err) {
			console.log(err);
			return false
		} else {
			return removedTask
		}
	})
}

//Updating a tasks

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}
		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr)=> {
			if(saveErr) {
				console.log(saveErr)
				return false
			} else {
				return updatedTask
			}
		})
	})
}
