//Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

//Set up server
const app = express();
//Assign Port
const port = 3000;
app.use(express.json());

//Database Connection MongoDB
mongoose.connect("mongodb+srv://admin:admin123@batch204-lagoyo.0azkaog.mongodb.net/B204-to-dos?retryWrites=true&w=majority",
{
	useNewUrlParser:true,
	useUnifiedTopology:true
}
);
//Set notifications for connection success or failuer
let db = mongoose.connection;

//If a connection error occured, output in the console
db.on("error",console.error.bind(console, "Connection Error"));
//if the connection is successful, output in the console
db.once("open", () => console.log("Were connected to the cloud database."));

//Add task route
app.use("/tasks", taskRoute);
//localhost:3000/tasks/



//Server Listening
app.listen(port, ()=> console.log(`Now listening to port ${port}`));
